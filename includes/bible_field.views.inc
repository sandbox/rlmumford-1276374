<?php

/**
 * Implements hook_field_views_data().
 */
function bible_field_field_views_data($field) {
  $data = field_views_field_default_views_data($field);
  
  // Add handler for books
  foreach ($field['storage']['details']['sql'] as $type => $tables) {
    foreach ($tables as $table_name => $columns) {
      if(!isset($columns['book'])) {
        continue;
      }
      
      $column_real_name = $columns['book'];
      if (!isset($data[$table_name][$column_real_name]['filter'])) {
        continue;
      }

      $data[$table_name][$column_real_name]['filter']['handler'] = 'bible_field_views_handler_filter_book';
    }
  }
  
  // Add handlers for chapter_verse ranges
  foreach ($field['storage']['details']['sql'] as $type => $tables) {
    foreach ($tables as $table_name => $columns) {
      if(!isset($columns['chapter_verse_range_start'])) {
        continue;
      }
      
      $column_real_name = $columns['chapter_verse_range_start'];
      if (!isset($data[$table_name][$column_real_name]['filter'])) {
        continue;
      }

      $data[$table_name][$column_real_name]['filter']['handler'] = 'bible_field_views_handler_filter_start_end';
    }
  }
  
  dsm("data ".print_r($data,true));
  dsm("field ".print_r($field,true));
  
  return $data;
}