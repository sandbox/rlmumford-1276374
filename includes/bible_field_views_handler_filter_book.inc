<?php

/**
 * A book filter - pulls in a select input with the currently selected bible CSV
 */
class bible_field_views_handler_filter_book extends views_handler_filter_in_operator {
  function get_value_options() {
    $this->value_title = t('Book');
    $field = field_info_field($this->definition['field_name']);
    $this->value_options = bible_field_book_options($field['settings']['version']);
  }
}